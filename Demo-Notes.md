# Demo Notes

## Following Along Setup

Before the workshop make sure you have:

* A GitLab account
* [Node.js](https://nodejs.org/en/)
* [Git Bash](https://git-scm.com/downloads)
* [Postman](https://www.getpostman.com/downloads/)
* Your favorite editor

## Set up a Node.js project

First, we will want a `package.json` that will keep track of our project dependencies and some meta data about our project.

**Step 1:** Create a `package.json` by navigating to the root of your project from the command line and entering `npm init`. Answer the prompts that follow. Now, you can download dependencies as needed during the development process with `npm install <dependency> --save`

> Advanced: add an object to your package.json with the key "scripts" to create shortcuts for common actions like "test" or "start"

Next, we will want to set up the folder structure and add the files that we will need for our app.

**Step 2:** Create the following folder structure:

``` sh
demo-app/
├── config/
│   └── config.js
├── lib/
│   └── hello-world.js
├── routes/
|   └── routes.js
└── app.js
```

> Advanced: add a `test` folder with a `hello-world-test.js` file and a `.gitlab-ci.yml`, `.gitignore`, and a `.eslintrc.json` to the root of the project as well.

After that, we will want to add an entry point for our application that listens for and handles requests.

**Step 3:** Create your http server in `app.js`

1. Add the require statements for `http`, `express`, and `routes`, but don't forget to install express with `npm install express --save`

``` JS
const http = require('http');
const express = require('express');
const config = require('./config/config');
const routes = require('./routes/routes');
```

2. Create an express app, tell your app what middleware to use, what routes to use, and create the  HTTP server

```JS
const app = express();    // create an express app
app.use(express.json());  // tell your app what middleware to use
app.use('/', routes);     // tell your app what routes to use
const httpServer = http.createServer(app); // create the http server
```

3. Tell your server to start listening on the desired port exposed, and log a message to the console

``` JS
httpServer.listen(3000);
console.log('[STARTUP] app is listening on port 3000');
```

> Advanced: Store the port in the `config` file and declare it in such a way that you can read in the port from the command line or the `config` file. Also, export the `httpServer` for testing.

Then, we will want to create the routes that your app will handle. We already require and use the routes in `app.js` so we will just need to create and export them in `routes.js`.

**Step 4:** Add a route to `routes.js`

1. Require express and the functionality you will need from your `lib` folder

``` JS
const express = require('express');
const helloWorld = require('../lib/hello-world');
```

2. Declare and instantiate your router

``` JS
const router = express.Router();
```

3. Tell your router what route to handle and how to respond

``` JS
router.get('/', (req, res) => {
  res.status(200).json(helloWorld());
});
````

4. Export the routes for your app to use

``` JS
module.exports = router;
```

Lastly, we will want `helloWorld()` to return an object that the route can respond with.

**Step 5:** Add functionality to your route in `hello-world.js` by exporting a function that your route can use

``` JS
module.exports = () => ({
  message: 'hello world',
});
```

**Step 6:** Add business requirements to your current application in `routes.js` with the included libraries

The business analyst on your team has drafted the following requirements for our student management system:

* Users should be able to add students
* Users should be able to retrieve students
* Users should be able to delete students
* Users should be able to add assignments to an existing student
* Users should be able to retrieve an assignment from a student
* Users should be able to delete an assignment from a student

To enable these pieces of functionality, we will use what we have learned about Node, REST and Express.

1. Add included libraries to your application

The three files we are going to use to mock out a student database are `student.js`, `assignment.js` and `student-database.js`. They are located in the `lib` folder at the base of this project. You will need to place them in a lib folder of your own.

`student-database.js` has six functions that correspond with the business requirements listed above:

``` JS
addStudent(firstName: string, lastName: string, classYear: number)
```
``` JS
getStudent(studentID: number)
```
``` JS
deleteStudent(studentID: number)
```
``` JS
addAssignment(name: string, grade: number)
```
``` JS
getAssignment(studentID: number, assignmentID: number)
```
``` JS
deleteAssignment(studentID: number, assignmentID, number)
```

All of these functions will handle creating, persisting and handling errors if a student or assignment does not exist. They will also return the objects they are manipulating when completed.

2. Create the student endpoints

* Create Student

We will need to create a `POST` endpoint with the `/student` resource. This endpoint will expect a HTTP request body that will have the following parameters:

``` JSON
{
  "firstName": "John",
  "lastName": "Jacob",
  "classYear": 2008
}
```

These parameters can be found in the `req.body` object in the request passed into our endpoint handler. We can then check to see that these parameters have values and pass them into our mock database function like so:

``` JS
router.post('/student', (req, res) => {
  const { firstName, lastName, classYear } = req.body;
  if (firstName && lastName && classYear) {
    const student = StudentDatabase.addStudent(firstName, lastName, classYear);
    res.status(200).json(student);
  } else {
    res.status(400).send({ error: `Error: Student is malformed` });
  }
});
```

If the request does not have all of the required parameters listed above, we will return a status code of `400 (Bad Request)` with the message `Error: Student is malformed`. Otherwise, we will return the created student with the status code `200 (OK)`.


* Retrieve Student

Now we will need to create another `GET` request to retrieve any student based on their ID. Since this is returned from the `POST` call outlined above, the user should have this information.

We will pass in the `studentID` number as a URL parameter for our student resource, so that our resource looks like `/student/:studentID`. URL parameters used can be found in the `req.params` object.

Then we can just use the database function to retrieve the student or return a `404` error if that student is not found:
``` JS
router.get('/student/:studentID', (req, res) => {
  try {
    const student = StudentDatabase.getStudent(Number(req.params.studentID));
    res.status(200).json(student);
  } catch (error) {
    res.status(404).send({ error });
  }
});
```
* Delete Student

The delete function will look just like the call above, except we will be calling `deleteStudent` instead of `getStudent`:

``` JS
router.delete('/student/:studentID', (req, res) => {
  try {
    const student = StudentDatabase.deleteStudent(Number(req.params.studentID));
    res.status(200).json(student);
  } catch (error) {
    res.status(404).send({ error });
  }
});
```

3. Create the assignment endpoints

* Create Assignment

This assignment endpoint will be very similar to the create student endpoint, but we will branch off of the student resource to access the assignment resource, As such, our new URI will be `/student/:studentID/assignment`.

This endpoint will expect a request body that will have the following parameters:

``` JSON
{
  "name": "Chapter 7, Section 2",
  "grade": 74
}
```

Additionally, we will need to handle two errors: a `404 (Not Found)` if the student resource does not exist, and a `400 (Bad Request)` error if the required parameters are not present.

We can handle both scenarios with the following:

``` JS
router.post('/student/:studentID/assignment', (req, res) => {
  const { name, grade } = req.body;
  try {
    if (name && grade) {
      const assignment = StudentDatabase.addAssignment(Number(req.params.studentID), name, grade);
      res.status(200).json(assignment);
    } else {
      res.status(400).send({ error: `Error: Assignment is malformed` });
    }
  } catch (error) {
    res.status(404).send({ error: error.message });
  }
});
```

* Retrieve Assignment

In order to retrieve an assignment, we will neded the ID of the student, as well as the ID of the assignment. As such, our URI will be `/student/:studentID/assignment/:assignmentID`.

Otherwise, this endpoint looks just like the retrieve student endpoint:

``` JS
router.get('/student/:studentID/assignment/:assignmentID', (req, res) => {
  try {
    const assignment = StudentDatabase.getAssignment(Number(req.params.studentID), Number(req.params.assignmentID));
    res.status(200).json(assignment);
  } catch (error) {
    res.status(404).send({ error: error.message });
  }
});
```

* Delete Assignment

Just like the above endpoint, this operation will have the URI of `/student/:studentID/assignment/:assignmentID`, but will have the `DELETE` operation.

This also looks like the delete student endpoint:

``` JS
router.delete('/student/:studentID/assignment/:assignmentID', (req, res) => {
  try {
    const assignment = StudentDatabase.deleteAssignment(Number(req.params.studentID), Number(req.params.assignmentID));
    res.status(200).json(assignment);
  } catch (error) {
    res.status(404).send({ error: error.message });
  }
});
```

And there you have it, all of the business requirements specified have been met!

You can manually test the functionality of these endpoints using a utility like [Postman](https://www.getpostman.com/downloads/) to send requests to each endpoint and asserting the responses match what you expect.
