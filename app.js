const http = require('http');
const express = require('express');
const config = require('./config/config');
const studentRoutes = require('./routes/student-routes');

const app = express();
app.use(express.json());
app.use('/', studentRoutes);
const httpServer = http.createServer(app);

// set the port depending on what the user puts at command line
const port = process.env.port || config.port || 3000;
httpServer.listen(port);
console.log(`[STARTUP] app is listening on port: ${port}`);

module.exports = {
  httpServer,
};
