const Student = require('./student');
const Assignment = require('./assignment');

// constructor for StudentDatabase Model
function StudentDatabase() { }
// Model attributes
const studentArray = [];
let studentsCreated = 0;

// Add functionality
StudentDatabase.prototype.getStudent = function (studentID) {
  const stud = studentArray.find(student => student.ID === studentID);
  if (!stud) {
    throw new Error(`Error: Student with ID ${studentID} does not exist.`);
  }
  return stud;
};

StudentDatabase.prototype.getAssignment = function (studentID, assignmentID) {
  const stud = this.getStudent(studentID);
  return stud.getAssignment(assignmentID);
};

StudentDatabase.prototype.addStudent = function (firstName, lastName, classYear) {
  const newStudent = new Student(firstName, lastName, classYear);
  newStudent.ID = studentsCreated;
  studentsCreated += 1;
  studentArray.push(newStudent);
  return newStudent;
};

StudentDatabase.prototype.addAssignment = function (studentID, name, grade) {
  const stud = this.getStudent(studentID);
  const assignment = new Assignment(name, grade);
  return stud.addAssignment(assignment);
};

StudentDatabase.prototype.deleteStudent = function (studentID) {
  const stud = this.getStudent(studentID);
  studentArray.splice(studentArray.indexOf(stud), 1);
  return stud;
};

StudentDatabase.prototype.deleteAssignment = function (studentID, assignmentID) {
  const stud = this.getStudent(studentID);
  return stud.deleteAssignment(assignmentID);
};

// export newly created MockDB
module.exports = new StudentDatabase();
