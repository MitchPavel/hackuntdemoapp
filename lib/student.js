function Student(firstName, lastName, classYear) {
  this.firstName = firstName;
  this.lastName = lastName;
  this.classYear = classYear;
  this.assignmentArray = [];
  this.addedAssignments = 0;
}

Student.prototype.addAssignment = function (assignment) {
  this.assignmentArray.push(assignment);
  this.assignmentArray[this.assignmentArray.length - 1].ID = this.addedAssignments;
  this.addedAssignments += 1;
  return assignment;
};

Student.prototype.getAssignment = function (assignmentID) {
  const assignment = this.assignmentArray.find(assign => assign.ID === assignmentID);
  if (!assignment) {
    throw new Error(`Error: Assignment with ID ${assignmentID} does not exist.`);
  }
  return assignment;
};

Student.prototype.deleteAssignment = function (assignmentID) {
  const assignment = this.getAssignment(assignmentID);
  this.assignmentArray.splice(this.assignmentArray.indexOf(assignment), 1);
  return assignment;
};

module.exports = Student;
