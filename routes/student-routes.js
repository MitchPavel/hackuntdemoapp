const express = require('express');
const StudentDatabase = require('../lib/student-database');

const router = express.Router();

router.get('/student/:studentID', (req, res) => {
  try {
    const student = StudentDatabase.getStudent(Number(req.params.studentID));
    res.status(200).json(student);
  } catch (error) {
    res.status(404).send({ error: error.message });
  }
});

router.get('/student/:studentID/assignment/:assignmentID', (req, res) => {
  try {
    const assignment = StudentDatabase.getAssignment(Number(req.params.studentID), Number(req.params.assignmentID));
    res.status(200).json(assignment);
  } catch (error) {
    res.status(404).send({ error: error.message });
  }
});

router.post('/student', (req, res) => {
  const { firstName, lastName, classYear } = req.body;
  if (firstName && lastName && classYear) {
    const student = StudentDatabase.addStudent(firstName, lastName, classYear);
    res.status(200).json(student);
  } else {
    res.status(400).send({ error: `Error: Student is malformed` });
  }
});

router.post('/student/:studentID/assignment', (req, res) => {
  const { name, grade } = req.body;
  try {
    if (name && grade) {
      const assignment = StudentDatabase.addAssignment(Number(req.params.studentID), name, grade);
      res.status(200).json(assignment);
    } else {
      res.status(400).send({ error: `Error: Assignment is malformed` });
    }
  } catch (error) {
    res.status(404).send({ error: error.message });
  }
});

router.delete('/student/:studentID', (req, res) => {
  try {
    const student = StudentDatabase.deleteStudent(Number(req.params.studentID));
    res.status(200).json(student);
  } catch (error) {
    res.status(404).send({ error: error.message });
  }
});

router.delete('/student/:studentID/assignment/:assignmentID', (req, res) => {
  try {
    const assignment = StudentDatabase.deleteAssignment(Number(req.params.studentID), Number(req.params.assignmentID));
    res.status(200).json(assignment);
  } catch (error) {
    res.status(404).send({ error: error.message });
  }
});

module.exports = router;
