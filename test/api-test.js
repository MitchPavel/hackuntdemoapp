/* eslint-env node, mocha */
/* eslint no-unused-expressions: off, global-require: off, prefer-destructuring: off */

process.env.NODE_ENV = 'test';

const chai = require('chai');
const chaiHttp = require('chai-http');

const { expect } = chai;
chai.use(chaiHttp);

describe('api', () => {
  let httpServer;
  beforeEach(() => {
    httpServer = require('../app').httpServer;
  });
  describe('A POST to /student will return the created student', () => {
    it('it should return created student', (done) => {
      chai.request(httpServer)
        .post('/student')
        .send({firstName: "John", lastName: "Jacob", classYear: 2004})
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res.body).to.be.eql({firstName: "John", lastName: "Jacob", classYear: 2004, ID: 0, assignmentArray: [], addedAssignments: 0});
          done();
        });
    });
  });
  describe('A POST to /student with malformed data will return 400', () => {
    it('it should 400 when first name is undefined', (done) => {
      chai.request(httpServer)
        .post('/student')
        .send({lastName: "Jacob", classYear: 2004})
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(400);
          done();
        });
    });
    it('it should return 400 when lastName is undefined', (done) => {
      chai.request(httpServer)
        .post('/student')
        .send({firstName: "John", classYear: 2004})
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(400);
          done();
        });
    });
    it('it should return 400 when classYear is undefined', (done) => {
      chai.request(httpServer)
        .post('/student')
        .send({firstName: "John", lastName: "Jacob"})
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(400);
          done();
        });
    });
  });
  describe('A GET to /student/:ID will return the student with ID', () => {
    it('it should return requested student', (done) => {
      chai.request(httpServer)
        .get(`/student/0`)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res.body).to.be.eql({firstName: "John", lastName: "Jacob", classYear: 2004, ID: 0, assignmentArray: [], addedAssignments: 0});
          done();
        });
    });
  });
  describe('A GET to /student/:ID will return 404 if the student doesn\'t exist', () => {
    it('it should return 404 for requested student', (done) => {
      chai.request(httpServer)
        .get(`/student/1`)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(404);
          done();
        });
    });
  });
  describe('A POST to /assignment will return the created student', () => {
    it('it should return created student', (done) => {
      chai.request(httpServer)
        .post('/student/0/assignment')
        .send({name: "Chapter 5, Section 3", grade: 87})
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res.body).to.be.eql({name: "Chapter 5, Section 3", grade: 87, ID: 0});
          done();
        });
    });
  });
  describe('A POST to /student/:ID/assignment with malformed data will return 400', () => {
    it('it should 400 when name is undefined', (done) => {
      chai.request(httpServer)
        .post('/student/0/assignment')
        .send({grade: 87})
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(400);
          done();
        });
    });
    it('it should return 400 when grade is undefined', (done) => {
      chai.request(httpServer)
        .post('/student/0/assignment')
        .send({name: "Chapter 5, Section 3"})
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(400);
          done();
        });
    });
  });
  describe('A POST to /student/:ID/assignment should return 404 if student doesn\'t exist', () => {
    it('it should 404 for requested assignment', (done) => {
      chai.request(httpServer)
        .post('/student/1/assignment')
        .send({name: "Chapter 5, Section 3", grade: 87})
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(404);
          done();
        });
    });
  });
  describe('A GET to /student/:ID/assignment/:ID will return the assignment with ID', () => {
    it('it should return requested assignment', (done) => {
      chai.request(httpServer)
        .get(`/student/0/assignment/0`)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res.body).to.be.eql({name: "Chapter 5, Section 3", grade: 87, ID: 0});
          done();
        });
    });
  });
  describe('A GET to /student/:ID/assignment/:ID will return 404 if the assignment doesn\'t exist', () => {
    it('it should 404 for requested assignment', (done) => {
      chai.request(httpServer)
        .get(`/student/0/assignment/1`)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(404);
          done();
        });
    });
  });
  describe('A DELETE to /student/:ID/assignment/:ID will delete the assignment with ID', () => {
    it('it should return deleted assignment', (done) => {
      chai.request(httpServer)
        .delete(`/student/0/assignment/0`)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res.body).to.be.eql({name: "Chapter 5, Section 3", grade: 87, ID: 0});
          done();
        });
    });
  });
  describe('A DELETE to /student/:ID will return 404 if the student is not found', () => {
    it('it should return deleted student', (done) => {
      chai.request(httpServer)
        .delete(`/student/0/assignment/0`)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(404);
          done();
        });
    });
  });
  describe('A DELETE to /student/:ID will delete the student with ID', () => {
    it('it should return deleted student', (done) => {
      chai.request(httpServer)
        .delete(`/student/0`)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(200);
          expect(res.body).to.be.eql({firstName: "John", lastName: "Jacob", classYear: 2004, ID: 0, assignmentArray: [], addedAssignments: 1});
          done();
        });
    });
  });
  describe('A DELETE to /student/:ID will return 404 if the student is not found', () => {
    it('it should return deleted student', (done) => {
      chai.request(httpServer)
        .delete(`/student/0`)
        .end((err, res) => {
          expect(err).to.be.null;
          expect(res).to.have.status(404);
          done();
        });
    });
  });
  afterEach(() => {
    httpServer.close();
  });
});
