/* eslint-env node, mocha */
/* eslint no-unused-expressions: off */

process.env.NODE_ENV = 'test';

const chai = require('chai');
const helloWorld = require('../../lib/hello-world');

const { expect } = chai;

describe('helloWorld', () => {
  describe('helloWorld', () => {
    it('it should return an object with "hello world"', (done) => {
      const actual = helloWorld();
      const expected = { message: 'hello world' };
      expect(actual).to.be.eql(expected);
      done();
    });
  });
});
